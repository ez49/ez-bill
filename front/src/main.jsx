import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter as Router } from 'react-router-dom';
import { ConfigProvider } from 'zarm';
import 'zarm/dist/zarm.css';
import zhCN from 'zarm/lib/config-provider/locale/zh_CN';
import App from './App.jsx';
import './index.css';
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Router>
      <ConfigProvider primaryColor="#3eaf7c" locale={zhCN}>
        <App />
      </ConfigProvider>
    </Router>
  </React.StrictMode>
);
