import Data from '@/pages/Data'
import Detail from '@/pages/Detail'
import Home from '@/pages/Home/index.jsx'
import Login from '@/pages/Login'
import User from '@/pages/User'
import UserInfo from '@/pages/UserInfo'

const routes = [
  { path: '/login', component: Login },
  { path: '/', component: Home },
  { path: '/data', component: Data },
  { path: '/user', component: User },
  { path: '/userInfo', component: UserInfo },
  { path: '/detail', component: Detail },
]

export default routes
