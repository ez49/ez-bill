import { typeMap } from '@/utils';
import dayjs from 'dayjs';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { List } from 'zarm';
import CustomIcon from '../CustomIcon';
import s from './style.module.less';

const BillItem = ({ bill }) => {
  const [income, setIncome] = useState(0);
  const [expense, setExpense] = useState(0);
  const navigateTo = useNavigate(); //路由实例
  // 前往账单详情
  const goToDetail = item => {
    navigateTo(`/detail?id=${item.id}`);
  };
  // 当添加账单时，bill.bills长度变化，触发当日收支总和计算
  useEffect(() => {
    // 初始化将传入的 bill 内的 bills 数组内数据项，过滤出支出和收入。
    // pay_type：1 为支出；2 为收入
    // 通过 reduce 累加
    const _income = bill.bills
      .filter(i => i.pay_type == 2)
      .reduce((curr, item) => {
        curr += Number(item.amount);
        return curr;
      }, 0);
    setIncome(_income);
    const _expense = bill.bills
      .filter(i => i.pay_type == 1)
      .reduce((curr, item) => {
        curr += Number(item.amount);
        return curr;
      }, 0);
    setExpense(_expense);
  }, [bill.bills]);

  return (
    <div className={s.item}>
      <div className={s.headerDate}>
        <div className={s.date}>{bill.date}</div>
        <div className={s.money}>
          <span>
            <img src="//s.yezgea02.com/1615953405599/zhi%402x.png" alt="支" />
            <span>￥{expense.toFixed(2)}</span>
          </span>
          <span>
            <img src="//s.yezgea02.com/1615953405599/shou%402x.png" alt="收" />
            <span>￥{income.toFixed(2)}</span>
          </span>
        </div>
      </div>
      <List>
        {bill &&
          bill.bills.map(item => (
            <List.Item
              className={s.bill}
              key={item.id}
              hasArrow={false}
              onClick={() => goToDetail(item)}
              title={
                <>
                  <CustomIcon
                    className={s.itemIcon}
                    type={item.type_id ? typeMap[item.type_id].icon : 1}
                  />
                  <span>{item.type_name}</span>
                </>
              }
              suffix={
                <span
                  style={{ color: item.pay_type == 2 ? 'red' : '#39be77' }}
                >{`${item.pay_type == 1 ? '-' : '+'}${item.amount}`}</span>
              }
              description={
                <div>
                  {dayjs(Number(item.date)).format('HH:mm')}{' '}
                  {item.remark ? `| ${item.remark}` : ''}
                </div>
              }
            ></List.Item>
          ))}
      </List>
    </div>
  );
};

BillItem.propTypes = { bill: PropTypes.object };

export default BillItem;
