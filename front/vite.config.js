import react from '@vitejs/plugin-react-swc'
import { fileURLToPath, URL } from 'node:url'
import pxtovw from 'postcss-px-to-viewport'
import { defineConfig } from 'vite'
import { createStyleImportPlugin } from 'vite-plugin-style-import'
const loder_pxtovw = pxtovw({
  viewportWidth: 375,
  viewportUnit: 'vw',
  exclude: [/node_modules\/zarm/i]
})
const zarm_pxtovw = pxtovw({
  viewportWidth: 375,
  viewportUnit: 'vw',
  exclude: [/^(?!.*node_modules\/zarm)/] //忽略除zarm之外的
})

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    proxy: {
      '/api': {
        target: 'http://127.0.0.1:7001',
        changeOrigin: true,
        rewrite: path => path.replace(/^\/api/, '')
      }
    }
  },
  css: {
    postcss: {
      plugins: [loder_pxtovw, zarm_pxtovw]
    },
    modules: {
      localsConvention: 'dashesOnly'
    },
    preprocessorOptions: {
      less: {
        javascriptEnabled: true
      }
    }
  },
  plugins: [react(), createStyleImportPlugin({
    // resolves: [
    //   ZarmResolve(),
    // ],
    libs: [
      {
        libraryName: 'zarm',
        esModule: true,
        resolveStyle: (name) => {
          return `zarm/es/${name}/style/css`
        }
      }
    ]
  })],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})
