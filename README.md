# ez-bill

移动端记账本 h5 应用

#### 安装教程

因为 react-captcha-code 目前绑定的 React 版本是 ^16.13，所以安装项目依赖时需要加上 --force 参数

```
npm install --force
```

#### 介绍

node + react 实现一个移动端记账本应用
![网页截图](./home.png)

<!-- ![网页截图](./login.png)  -->

项目不大，但是本项目包含了几个关键的知识点：

后端知识点：

- 多用户鉴权
- 一套增删改查
- 表数据的二次处理（图表）
- 列表分页的制作
- 文件数据上传

前端知识点：

- 移动端适配

#### 软件架构

- 前端：vite + React18 + ZarmUI
- 后端：eggjs + MySQL
