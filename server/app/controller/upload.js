'use stric';
const fs = require('fs');
const moment = require('moment');
const mkdirp = require('mkdirp');
const path = require('path');

const Controller = require('egg').Controller;

class UploadController extends Controller {
  async upload() {
    const { ctx } = this;
    // ctx.request.files[0]表示获取第一个文件，若前端上传多个文件则可以遍历这个数组对象
    const file = ctx.request.files[0];
    // 声明资源存放路径
    let uploadDir = '';
    try {
      const f = fs.readFileSync(file.filepath);
      const day = moment(new Date()).format('YYYYMMDD');
      // 创建图片保存路径
      const dir = path.join(this.config.uploadDir, day);
      const date = Date.now();
      await mkdirp(dir);// 不存在就创建目录
      uploadDir = path.join(dir, date + path.extname(file.filename));
      fs.writeFileSync(uploadDir, f);
    } finally {
      ctx.cleanupRequestFiles();
    }
    ctx.body = {
      code: 200,
      msg: '上传成功',
      data: uploadDir.replace(/app/g, ''),
    };
  }
}

module.exports = UploadController;
