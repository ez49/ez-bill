CREATE DATABASE `ez_cost` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT ENCRYPTION='N';

create TABLE ez_cost.`user` (
	id int(11) auto_increment NOT NULL,
	username varchar(100) NOT NULL,
	ctime varchar(100) NOT NULL,
	avatar varchar(100) NOT NULL,
	signature varchar(100) NULL,
	password varchar(100) NOT NULL,
	primary key(id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci
AUTO_INCREMENT=1;

create TABLE ez_cost.`bill` (
	id int(11) auto_increment NOT NULL,
	pay_type int(11) NOT NULL,
	amount varchar(100) NOT NULL,
	date varchar(100) NOT NULL,
	type_id int NULL,
	type_name varchar(100) NOT NULL,
  user_id int NULL,
  remark varchar(100) NOT NULL,
	primary key(id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci
AUTO_INCREMENT=0;

CREATE TABLE `type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `type` int NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='标签表';
